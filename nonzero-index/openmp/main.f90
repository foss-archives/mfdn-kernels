#ifdef TARGET_GPU_LOOP
module m_kernel
  implicit none

contains

  subroutine index_nonzero_elements(n, m, p, arr, dt)
    ! ref. CountNonzeroTilesNN in module_TileSparsit.f90
    use iso_fortran_env, only : dp => real64, i8 => int64, i2 => int8
    use m_timer, only : wtime
    integer, intent(in) :: n, m, P
    integer, intent(out) :: arr(n*m)
    integer :: indx(n)

    integer :: i,j,k

    real(dp), intent(out) :: dt
    real(dp) :: t0

    !$omp target data map(alloc:indx) map(from:arr)
    t0 = wtime()
#ifdef USE_BIND
    !$omp target teams loop bind(teams)
#else
    !$omp target teams loop
#endif
    do i = 1, n
       indx(i) = (i-1)*m
#ifdef USE_BIND
       !$omp loop bind(parallel) private(k)
#else
       !$omp loop private(k)
#endif
       do j = 1, m
          if (mod(j,p) == 0) then
             !$omp atomic capture
             indx(i) = indx(i) + 1
             k = indx(i)
             !$omp end atomic
             arr(k) = j
          end if
       end do
       !$omp end loop
    end do
    !$omp end target teams loop
    dt = wtime() - t0
    !$omp end target data

  end subroutine index_nonzero_elements
end module m_kernel
#endif

#ifdef TARGET_GPU
module m_kernel
  implicit none
    
contains

  subroutine index_nonzero_elements(n, m, p, arr, dt)
    ! ref. CountNonzeroTilesNN in module_TileSparsit.f90
    use iso_fortran_env, only : dp => real64, i8 => int64, i2 => int8
    use m_timer, only : wtime
    integer, intent(in) :: n, m, p
    integer, intent(out) :: arr(n*m)
    integer :: indx(n)

    integer :: i,j,k
    
    real(dp), intent(out) :: dt
    real(dp) :: t0

    !$omp target data map(from:indx) map(from:arr)
    t0 = wtime()
    !$omp target teams distribute shared(arr, indx)
    do i = 1, n
       indx(i) = (i-1)*m
       !$omp parallel do private(k) shared(arr, indx)
       do j = 1, m
          if (mod(j,p) == 0) then
             !$omp atomic capture
             indx(i) = indx(i) + 1
             k = indx(i)
             !$omp end atomic
             arr(k) = j
          end if
       end do
       !$omp end parallel do
    end do
    !$omp end target teams distribute
    dt = wtime() - t0
    !$omp end target data
        
  end subroutine index_nonzero_elements
end module m_kernel
#endif

#ifdef TARGET_CPU
module m_kernel
contains
  subroutine index_nonzero_elements(n,m,p,arr)
    integer, intent(in) :: n, m, p
    integer, intent(out) :: arr(n*m)
    integer :: indx(n)
    integer :: i,j,k

    arr(:) = 0

    !$omp parallel do private(k)
    do i = 1, n
       indx(i) = (i-1)*m
       do j = 1, m
          if (mod(j,p) == 0) then
             indx(i) = indx(i) + 1
             k = indx(i)
             arr(k) = j
          end if
       end do
    end do

  end subroutine index_nonzero_elements
end module m_kernel
#endif

#ifdef INLINE_METADIRECTIVE
module m_kernel
contains
  subroutine index_nonzero_elements(n,m,p,arr)
    integer, intent(in) :: n, m, p
    integer, intent(out) :: arr(n*m)
    integer :: indx(n)
    integer :: i,j,k

    arr(:) = 0

#ifdef _OPENMP
    !$omp target data map(alloc:indx) map(from:arr)
    !$omp metadirective when(target_device={kind(gpu)}: target teams distribute) &
    !$omp& default(parallel do private(k))
#endif
    do i = 1, n
       indx(i) = (i-1)*m
#ifdef _OPENMP
       !$omp metadirective when(device={kind(gpu)}: parallel do private(k))
#endif
       do j = 1, m
          if (mod(j,p) == 0) then
#ifdef _OPENMP
             !$omp begin metadirective when(device={kind(gpu)}: atomic capture)
#endif
             indx(i) = indx(i) + 1
             k = indx(i)
#ifdef _OPENMP
             !$omp end metadirective
#endif
             arr(k) = j
          end if
       end do
    end do
#ifdef _OPENMP
    !$omp end target data
#endif

  end subroutine index_nonzero_elements
end module m_kernel
#endif

#ifdef DECLARE_VARIANT
module m_kernel
contains
  subroutine index_nonzero_elements(n,m,p,arr)
    integer, intent(in) :: n, m, p
    integer, intent(out) :: arr(n*m)
    integer :: indx(n)
    integer :: i,j,k

    arr(:) = 0
    
#ifdef _OPENMP
    !$omp target data map(alloc:indx) map(from:arr)
    !$omp metadirective when(target_device={kind(gpu)}: target teams distribute) &
    !$omp& default(parallel do private(k))
#endif
    do i = 1, n
       indx(i) = (i-1)*m
#ifdef _OPENMP
       !$omp metadirective when(device={kind(gpu)}: parallel do private(k))
#endif
       do j = 1, m
          if (mod(j,p) == 0) then
             call update_arr(indx, arr, n, m, i, j)
          end if
       end do
    end do
#ifdef _OPENMP
    !$omp end target data
#endif

  end subroutine index_nonzero_elements

  subroutine update_arr(indx, arr, n, m, i, j)
#ifdef _OPENMP
  !$omp declare variant(update_arr_gpu) match(device={kind(gpu)})
#endif
    integer, intent(in) :: i, j, n, m
    integer, intent(inout) :: indx(n), arr(n*m)
    indx(i) = indx(i) + 1
    k = indx(i)
    arr(k) = j
  end subroutine update_arr

  subroutine update_arr_gpu(indx, arr, n, m, i, j)
    integer, intent(in) :: i, j, n, m
    integer, intent(inout) :: indx(n), arr(n*m)
    !$omp atomic capture
    indx(i) = indx(i) + 1
    k = indx(i)
    !$omp end atomic
    arr(k) = j
  end subroutine update_arr_gpu

end module m_kernel
#endif



program main
  use m_kernel, only : index_nonzero_elements
  use iso_fortran_env, only : dp => real64, i2 => int8, i8 => int64
  implicit none  
  integer :: pnmax = 20
  integer :: pmmax = 10
  integer :: niter = 5
  integer :: i,j,n,m,p
  integer, allocatable :: arr(:)
  
  real(dp) :: dt
  integer :: nargs
  character(len=32) :: arg

  nargs = command_argument_count()
  if (nargs >= 1) then
     if (nargs >= 1) then
        call get_command_argument(1, arg)
        read(arg, *) pnmax
     end if
     if (nargs >= 2) then
        call get_command_argument(2, arg)
        read(arg, *) pmmax
     end if
     if (nargs >= 3) then
        call get_command_argument(3, arg)
        read(arg, *) niter
     end if
  end if

  write(*,'(*(g0, :, ","))') "iteration", "n", "m", "p", "time"

  n = 4
  do while(n < 2**pnmax)
     n = n * 2
     m = 4
     do while (m < 2**pmmax)        
        m = m * 2
        allocate(arr(n*m))
        !$omp target enter data map(alloc:arr)
        do p = 1, 4 ! fraction of inner most iterations that trigger the if condition
           do i = 1, niter
              call index_nonzero_elements(n, m, p, arr, dt)
#ifdef VALIDATE
              do j = p, m, p
                 if (count(arr == j) /= n) then
                    write(*,*) "wrong number of elements",count(arr == j),n
                    stop "did not validate"
                 end if
              end do
#endif
              write(*, '(*(g0, :, ","))') i, n, m, p, dt
           end do
        end do
        !$omp target exit data map(delete:arr)
        deallocate(arr)     
     end do
  end do
     
  
end program main
  
