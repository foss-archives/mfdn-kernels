
module m_kernel
  implicit none
    
contains

  subroutine index_nonzero_elements(n, m, p, arr, dt)
    ! ref. CountNonzeroTilesNN in module_TileSparsit.f90
    use iso_fortran_env, only : dp => real64, i8 => int64, i2 => int8
    use m_timer, only : wtime
    integer, intent(in) :: n, m, P
    integer, intent(out) :: arr(n*m)
    integer :: indx(n)

    integer :: i,j,k
    
    real(dp), intent(out) :: dt
    real(dp) :: t0

    !$acc data create(indx) copyout(arr)
    t0 = wtime()
    !$acc parallel loop present(indx, arr)
    do i = 1, n
       indx(i) = (i-1)*m
#ifdef _CRAYFTN
       !$acc loop
#else
       !$acc loop device_type(host) seq
#endif
       do j = 1, m
          if (mod(j,p) == 0) then
#ifndef NO_CPU_ATOMIC
             !$acc atomic capture
#endif
             indx(i) = indx(i) + 1
             k = indx(i)
#ifndef NO_CPU_ATOMIC
             !$acc end atomic
#endif
             arr(k) = j
          end if
       end do
       !$acc end loop
    end do
    !$acc end parallel loop
    dt = wtime() - t0
    !$acc end data
        
  end subroutine index_nonzero_elements


  subroutine index_nonzero_elements_cpu(n, m, p, arr, dt)
    ! ref. CountNonzeroTilesNN in module_TileSparsit.f90
    use iso_fortran_env, only : dp => real64, i8 => int64, i2 => int8
    use m_timer, only : wtime
    integer, intent(in) :: n, m, P
    integer, intent(out) :: arr(n*m)
    integer :: indx(n)

    integer :: i,j,k
    
    real(dp), intent(out) :: dt
    real(dp) :: t0

    t0 = wtime()
    do i = 1, n
       indx(i) = (i-1)*m
       do j = 1, m
          if (mod(j,p) == 0) then
             indx(i) = indx(i) + 1
             k = indx(i)
             arr(k) = j
          end if
       end do
    end do
    dt = wtime() - t0
        
  end subroutine index_nonzero_elements_cpu

  
end module m_kernel


program main
  use m_kernel
  use iso_fortran_env
  implicit none  
  integer :: pnmax = 20
  integer :: pmmax = 10
  integer :: niter = 20
  integer :: i,j,n,m,p
  integer, allocatable :: arr(:)
  integer(int64) :: check
  real(real64) :: dt
  integer :: nargs
  character(len=32) :: arg

  nargs = command_argument_count()
  if (nargs >= 1) then
     if (nargs >= 1) then
        call get_command_argument(1, arg)
        read(arg, *) pnmax
     end if
     if (nargs >= 2) then
        call get_command_argument(2, arg)
        read(arg, *) pmmax
     end if
     if (nargs >= 3) then
        call get_command_argument(3, arg)
        read(arg, *) niter
     end if
  end if

  write(*,'(*(g0, :, ","))') "iteration", "n", "m", "p", "time"  

  n = 4
  do while(n < 2**pnmax)
     n = n * 2
     m = 4
     do while (m < 2**pmmax)
        m = m * 2
        allocate(arr(n*m))
        !$acc enter data create(arr)             
        do p = 1, 4 ! fraction of inner most iterations that trigger the if condition

           call index_nonzero_elements_cpu(n,m,p,arr,dt)
           check = sum(int(arr, kind=8))
           
           do i = 1, niter
              call index_nonzero_elements(n, m, p, arr, dt)
              !$acc update host(arr)
              if (check /= sum(int(arr, kind=8))) then
                 write(*,*) "WRONG ANSWER"
                 stop
              else
                 write(*, '(*(g0, :, ","))') i, n, m, p, dt
              end if
           end do
        end do
        !$acc exit data delete(arr)
        deallocate(arr)
     end do
  end do
     
  
end program main
  
