# MFDN kernels

* `array-reduction` [^1]
* `nonzero-count` [^1]
* `nonzero-offset` [^1]
* `nonzero-index` [^1]
* `tasks-cpu-gpu`

[^1]: WACCPD 21: Accelerating quantum many-body configuration interaction with directives https://arxiv.org/abs/2110.10765 

## Legal

MFDn Kernels Copyright (c) 2021, The Regents of the University of 
|California, through Lawrence Berkeley National Laboratory (subject to 
receipt of any required approvals from the U.S. Dept. of Energy). All 
rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative 
works, and perform publicly and display publicly, and to permit others to do so.

## References
