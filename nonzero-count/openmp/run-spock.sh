#!/bin/bash
#SBATCH --dependency singleton
#SBATCH --job-name omp-count
#SBATCH --time 60
#SBATCH --nodes 1
#SBATCH --ntasks 1
#SBATCH --gpus 1

srun -u -n 1 --gpu-bind=closest ./benchmark.cray.mi100.x | tee cray.openmp.mi100.csv
