#!/bin/bash
#SBATCH --qos regular
#SBATCH --exclusive
#SBATCH --constraint dgx
#SBATCH --gpus 1
#SBATCH --time-min 60
#SBATCH --time 240

srun -n 1 --cpu-bind cores --sockets 1 -c 64 ./benchmark.nvidia.a100.use_bind.x | tee nvidia.openmp-loop-bind.a100.csv
srun -n 1 --cpu-bind cores --sockets 1 -c 64 ./benchmark.nvidia.a100.x | tee nvidia.openmp-loop.a100.csv

