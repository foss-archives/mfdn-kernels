module m_kernel
  implicit none
    
contains

  subroutine count_nonzero(n, np, bitrep1, bitrep2, mbstate1, mbstate2, numnnz, dt)
    use iso_fortran_env
    use m_timer, only : wtime
    integer, intent(in) :: n, np
    integer(int64), dimension(n), intent(in) :: bitrep1, bitrep2
    integer(int16), dimension(np,n), intent(in) :: mbstate1, mbstate2
    integer(int64), intent(out) :: numnnz
    real(real64), intent(out) :: dt
    integer :: i, j, d
    integer(int64) :: c
    integer(int64), dimension(n) :: counts
    real(real64) :: t0
    !$omp target data map(from:counts)
    t0 = wtime()
#ifdef USE_BIND
    !$omp target teams loop bind(teams) private(c)
#else
    !$omp target teams loop private(c)
#endif
    do i = 1, n
       c = 0
#ifdef USE_BIND
       !$omp loop bind(parallel) reduction(+:c) private(d)
#else
       !$omp loop reduction(+:c) private(d)
#endif       
       do j = 1, n
          d = popcnt(ieor(bitrep1(i), bitrep2(j)))
          if (d > 4) cycle
          d = count_difference(mbstate1(:,i), np, mbstate2(:,j), np)
          if (d <= 4) c = c + 1
       end do
       !$omp end loop
       counts(i) = c
    end do
    !$omp end target teams loop
    dt = wtime() - t0
    !$omp end target data
    numnnz = sum(counts)
  end subroutine count_nonzero


  subroutine count_nonzero_bitrep_only(n, bitrep1, bitrep2, bitrep_ext1, bitrep_ext2, numnnz, dt)
    ! ref. CountNonzeroTilesNN in module_TileSparsity.f90
    use iso_fortran_env
    use m_timer, only : wtime
    integer, intent(in) :: n
    integer(int64), dimension(n), intent(in) :: bitrep1, bitrep2
    integer(int64), dimension(n), intent(in) :: bitrep_ext1, bitrep_ext2
    integer(int64), intent(out) :: numnnz
    real(real64), intent(out) :: dt
    integer :: i, j, d
    integer(int64) :: c
    integer(int64), dimension(n) :: counts
    real(real64) :: t0   
    !$omp target data map(from:counts)
    t0 = wtime()
#ifdef USE_BIND
    !$omp target teams loop bind(teams) private(c)
#else
    !$omp target teams loop private(c)
#endif
    do i = 1, n
       c = 0
#ifdef USE_BIND
       !$omp loop bind(parallel) reduction(+:c) private(d)
#else
       !$omp loop reduction(+:c) private(d)
#endif
       do j = 1, n
          d = popcnt(ieor(bitrep1(i), bitrep2(j))) + popcnt(ieor(bitrep_ext1(i), bitrep_ext2(j)))
          if (d <= 4) c = c + 1
       end do
       !$omp end loop
       counts(i) = c
    end do
    !$omp end target teams loop
    dt = wtime() - t0
    !$omp end target data
    numnnz = sum(counts)
  end subroutine count_nonzero_bitrep_only

  subroutine count_nonzero_no_bitrep(n, np, mbstate1, mbstate2, numnnz, dt)
    ! ref. CountNonzeroTilesNN in module_TileSparsity.f90
    use iso_fortran_env
    use m_timer, only : wtime
    integer, intent(in) :: n, np
    integer(int16), dimension(np,n), intent(in) :: mbstate1, mbstate2
    integer(int64), intent(out) :: numnnz
    real(real64), intent(out) :: dt
    integer :: i, j, d
    integer(int64) :: c
    integer(int64), dimension(n) :: counts
    real(real64) :: t0    
    !$omp target data map(from:counts)
    t0 = wtime()
#ifdef USE_BIND
    !$omp target teams loop bind(teams) private(c)
#else
    !$omp target teams loop private(c)
#endif
    do i = 1, n
       c = 0
#ifdef USE_BIND
       !$omp loop bind(parallel) reduction(+:c) private(d)
#else
       !$omp loop reduction(+:c) private(d)
#endif
       do j = 1, n
          d = count_difference(mbstate1(:,i), np, mbstate2(:,j), np)
          if (d <= 4) c = c + 1
       end do
       !$omp end loop
       counts(i) = c
    end do
    !$omp end target teams loop
    dt = wtime() - t0
    !$omp end target data
    numnnz = sum(counts)
  end subroutine count_nonzero_no_bitrep

  function count_difference(s1, n1, s2, n2)
    use iso_fortran_env
    integer :: count_difference
    integer, intent(in) :: n1, n2
    integer(int16), intent(in) :: s1(n1), s2(n2)
    integer :: i1, i2, d, diffs1, diffs2
    !$omp declare target
    i1 = 1
    i2 = 1
    diffs1 = 0
    diffs2 = 0
    do
       if ( (i1 > n1) .or. (i2 > n2) ) then
          exit
       end if
       d = s1(i1) - s2(i2)
       if (d < 0) then
          diffs1 = diffs1 + 1
          i1 = i1 + 1
       else if (d > 0) then
          diffs2 = diffs2 + 1
          i2 = i2 + 1
       else
          i1 = i1 + 1
          i2 = i2 + 1
       end if
    end do
    count_difference = 2*max(diffs1, diffs2) !diffs1 + diffs2
  end function count_difference
  
end module m_kernel


program main
  use iso_fortran_env
  use m_mbstate, only : create_state
  use m_kernel, only : count_nonzero, count_nonzero_no_bitrep, count_nonzero_bitrep_only
  implicit none  
  integer :: pnmax = 18
  integer :: niter = 5
  integer :: i,j,n,np
  integer(int64) :: numnnz, numnnz_detail, numnnz_bitrep

  integer(int16), dimension(:,:), allocatable :: mbstate1, mbstate2
  integer(int64), dimension(:), allocatable :: bitrep1, bitrep2
  integer(int64), dimension(:), allocatable :: bitrep_ext1, bitrep_ext2
  real(real64) :: dt, dt_bitrep, dt_detail
  integer :: nargs
  character(len=32) :: arg

  nargs = command_argument_count()
  if (nargs >= 1) then
     if (nargs >= 1) then
        call get_command_argument(1, arg)
        read(arg, *) pnmax
     end if
     if (nargs >= 2) then
        call get_command_argument(2, arg)
        read(arg, *) niter
     end if
  end if
    
  write(*,'(*(g0, :, ","))') "iteration", "n", "nparticles", "routine", "nnz", "time"

  n = 2
  do while(n < 2**pnmax)
     n = n * 2
     do np = 4, 20, 4
        allocate(bitrep1(n))
        allocate(bitrep2(n))
        allocate(bitrep_ext1(n))
        allocate(bitrep_ext2(n))
	allocate(mbstate1(np, n))
        allocate(mbstate2(np, n))
        !$omp target enter data map(alloc: bitrep1, bitrep2, bitrep_ext1, bitrep_ext2, mbstate1, mbstate2)
        do i = 1, niter
           do j = 1, n
              call create_state(np, mbstate1(:,j), bitrep1(j), bitrep_ext1(j))
           end do
           !$omp target update to(bitrep1, bitrep_ext1, mbstate1) nowait
           do j = 1, n
              call create_state(np, mbstate2(:,j), bitrep2(j), bitrep_ext2(j))
           end do
           !$omp target update to(bitrep2, bitrep_ext2, mbstate2) nowait
           !$omp taskwait
           
           call count_nonzero_bitrep_only(n, bitrep1, bitrep2, bitrep_ext1, bitrep_ext2, numnnz_bitrep, dt_bitrep)
           call count_nonzero_no_bitrep(n, np, mbstate1, mbstate2, numnnz_detail, dt_detail)
           call count_nonzero(n, np, bitrep1, bitrep2, mbstate1, mbstate2, numnnz, dt)

           if (numnnz /= numnnz_detail) then
              write(*,*) "WRONG: detail count does not match combined", numnnz, numnnz_detail
              stop
           end if
           if (numnnz /= numnnz_bitrep) then
              write(*,*) "WRONG: detail count does not match bitrep", numnnz_bitrep, numnnz_detail
              stop
           end if
           
           write(*, '(*(g0, :, ","))') i, n, np, "bitrep_only", numnnz, dt_bitrep
           write(*, '(*(g0, :, ","))') i, n, np, "detail_only", numnnz, dt_detail
           write(*, '(*(g0, :, ","))') i, n, np, "combined", numnnz, dt

        end do
        !$omp target exit data map(delete: bitrep1, bitrep2, bitrep_ext1, bitrep_ext2, mbstate1, mbstate2)
        deallocate(bitrep1, bitrep2, bitrep_ext1, bitrep_ext2, mbstate1, mbstate2)
     end do
  end do
     
end program main
  
