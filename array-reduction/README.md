# Array reduction

## Compile

Requires [fypp](https://fypp.readthedocs.io/en/stable/) for `manual` version.

```
for v in manual atomic; do for t in a100 v100 skylake; do make $v TARGET=$t; done; done
```
