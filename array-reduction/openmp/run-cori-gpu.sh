#!/bin/bash
#SBATCH --qos special
#SBATCH --exclusive
#SBATCH --constraint gpu
#SBATCH --gpus 1
#SBATCH --time-min 60
#SBATCH --time 240

for t in v100 skylake; do
    for v in array manual atomic; do 	
	srun -n 1 --cpu-bind cores --sockets 1 -c 40 ./benchmark.nvidia.$t.$v.x | tee nvidia.openmp.$t.$v.csv
    done
done
